/*global $, document, setTimeout*/
var objects = [];
//@DECLARE
var callJ = function (url) {
    'use strict';
    $(document).ready(function () {
        $.getJSON(url + "?callback=?", null, function (data) {
            var i;
            for (i in data) {
                if (data.hasOwnProperty(i)) {
                    if (i === 'next') {
                        callJ(data[i]);
                    } else if (i === 'values') {
                        objects.push(data[i]);
                    }
                }
            }
        });
    });
};
//@RETRIVING JSON
var url = "https://bitbucket.org/api/2.0/repositories/Mc01";
callJ(url);
//@MODIFICATION OF HTML
setTimeout(function () {
    'use strict';
    $('#loader').hide();
    var gridy = "<ul class='small-block-grid-1 medium-block-grid-3 large-block-grid-4'>";
    objects.forEach(function (entry) {
        entry.forEach(function (item) {
            var desc = item.name + "<br/>"
                + item.language.charAt(0).toUpperCase() + item.language.slice(1) + "<br/>"
                + item.updated_on.slice(0, 10) + "<br/>"
                + item.created_on.slice(0, 10);
            gridy += "<li><img data-tooltip class='th' style='display:block; margin-left:auto; margin-right:auto' src='" + item.links.avatar.href + "' title='" + desc + "'/></li>";
        });
    });
    gridy += "</ul>";
    document.getElementById('modifier').innerHTML = gridy;
    $(document).foundation();
    $('#modifier').show();
}, 2000);